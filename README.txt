==========================
Academic Torrents Client
==========================

Author:
    Ronald Barrios

==========================
Installation Instructions:
==========================

Build, compile C libraries, and install by running the bash script:

    $ install.sh

==========================
Contact/Support:
==========================

   email: ronald.degmar@gmail.com