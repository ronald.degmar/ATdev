import six, urllib, os, tarfile, sys, logging, site
import subprocess as sub
import shutil as sh

def getpath():
    if ('--user' in sys.argv):        
        six.print_(site.getusersitepackages())
        six.print_(site.getsitepackages())
        six.print_(sys.prefix)
        six.print_(sys.version_info[0])
        six.print_(sys.version_info[1])

def message(code, phase):
    if (code == 0):
        six.print_(phase + " successful")
    else:
        six.print_(phase+ " with errors. See install.log")    

def log_subprocess_output(pipe):
    for line in iter(pipe.readline, b''): # b'\n'-separated lines
        logging.info('%r', line)
        
def installat():
    AT_HOME = os.path.join(os.environ['HOME'],".local")
    os.chdir(AT_HOME)
    logging.basicConfig(filename='install.log', filemode='w', level=logging.DEBUG)
    PKG = os.path.join(os.environ['HOME'],"at-pkg")
    os.mkdir(PKG)
    os.chdir(PKG)    

    six.print_("Preparing Boost")
    _BOOST_URL = 'https://sourceforge.net/projects/boost/files/boost/1.63.0/boost_1_63_0.tar.gz'
    _BOOST_TB = 'boost_1_63_0.tar.gz'
    _BOOST_DIR='boost_1_63_0'
    urllib.urlretrieve(_BOOST_URL, _BOOST_TB)
    
    six.print_("Preparing Libtorrent")
    _LIBTORRENT_URL='https://github.com/arvidn/libtorrent/releases/download/libtorrent-1_0_10/libtorrent-rasterbar-1.0.10.tar.gz'    
    _LIBTORRENT_TB = 'libtorrent-rasterbar-1.0.10.tar.gz'
    _LIBTORRENT_DIR = 'libtorrent-rasterbar-1.0.10'
    urllib.urlretrieve(_LIBTORRENT_URL, _LIBTORRENT_TB)
    
    six.print_("Uncompressing libraries")
    for lib in [_BOOST_TB,_LIBTORRENT_TB]:
        six.print_("Uncompressing " + lib)
        tar = tarfile.open(lib)
        tar.extractall()
    tar.close()
    WITH_PYTHON = os.popen('which python2.7').read()
    six.print_("Python executable to use is: " + WITH_PYTHON)
    
    six.print_("Bootstrapping Boost")
    os.chdir(os.path.join(PKG,_BOOST_DIR))
    
    cmd = ['./bootstrap.sh','--with-python=%s'%WITH_PYTHON,'--prefix=%s'%AT_HOME]
    
    p = sub.Popen(cmd, stdout=sub.PIPE)
    with p.stdout:
        log_subprocess_output(p.stdout)    
    p.wait()
    message(p.returncode,six.text_type("Bootstrap"))
    
    six.print_("Building Boost C++ libraries")
    cmd = ['./b2','-s','NO_BZIP2=1','--with-system','--with-date_time','--with-python','--with-chrono',
           '--with-random','install','--prefix=%s'%AT_HOME]
    
    p = sub.Popen(cmd, stdout=sub.PIPE, stderr=sub.STDOUT)
    with p.stdout:
        log_subprocess_output(p.stdout)
    p.wait()
    message(p.returncode,six.text_type("Building"))
    
    six.print_("Configuring Libtorrent")
    os.chdir(os.path.join(PKG,_LIBTORRENT_DIR))
    cmd = ['./configure','--enable-python-binding','--disable-dependency-tracking','--disable-silent-rules',
           'PYTHON=%s'%WITH_PYTHON,'--prefix=%s'%AT_HOME,'--with-boost=%s'%AT_HOME,'--with-libiconv',
           '--with-boost-python=boost_python','BOOST_ROOT=%s'%os.path.join(PKG,_BOOST_DIR)]
        
    p = sub.Popen(cmd, stdout=sub.PIPE, stderr=sub.STDOUT)
    with p.stdout:
        log_subprocess_output(p.stdout)
    p.wait()
    message(p.returncode,six.text_type("Configuring Libtorrent"))
    
    six.print_("Making Libtorrent")
    os.chdir(os.path.join(PKG,_LIBTORRENT_DIR))
    p = sub.Popen(['make'], stdout=sub.PIPE, stderr=sub.STDOUT)
    with p.stdout:
        log_subprocess_output(p.stdout)
    p.wait()
    message(p.returncode,six.text_type("Making Libtorrent"))
    
    six.print_("Make install Libtorrent")
    os.chdir(os.path.join(PKG,_LIBTORRENT_DIR))
    p = sub.Popen(['make', 'install'], stdout=sub.PIPE, stderr=sub.STDOUT)
    with p.stdout:
        log_subprocess_output(p.stdout)
    p.wait()
    message(p.returncode,six.text_type("Making install Libtorrent"))  
    
    sh.rmtree(PKG)
    six.print_("Building of all C++ libraries finished")
    
    six.print_("Creating .at_profile")
    with open(os.path.join(os.environ['HOME'],".at_profile"), "w") as outfile:
        outfile.write("export LD_LIBRARY_PATH=" + os.path.join(AT_HOME,"lib") + "\n")
        outfile.write("export PKG_CONFIG_PATH=" + os.path.join(AT_HOME,"lib","pkgconfig")) 
    
    six.print_("Finished custom install")
    

def test():
    #AT_PROFILE = os.path.join(os.environ['HOME'],".at_profile")
    #with open(AT_PROFILE,'r') as f:
        #for line in f:
            #env = line.rstrip().split("=")
            #os.environ[env[0]]=env[1]
    
    #six.print_(os.environ['LD_LIBRARY_PATH'])
    AT_HOME = os.path.join(os.environ['HOME'],".local")
    with open(os.path.join(os.environ['HOME'],".at_profile"), "a+") as outfile:
        outfile.write("export LD_LIBRARY_PATH=" + os.path.join(AT_HOME,"lib") + "\n")
        outfile.write("export PKG_CONFIG_PATH=" + os.path.join(AT_HOME,"lib","pkgconfig"))         
        

if __name__ == "__main__":
    #test()
    installat()
    #getpath()