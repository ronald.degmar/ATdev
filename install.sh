#!/bin/bash

# Academic Torrents installer build script

# Path for install of Academic Torrents
AT_HOME="$HOME/AcademicTorrentsApp"

# Load Constant Definitions from library
[ -z "$_definitions" ] && . ./definitions

# Add Installer Messages
[ -z "$_messages" ] && . ./messages

# Set current directory and log variable defaults
ORIGIN_PATH=`pwd`
export ORIGIN_PATH
INSTALL_LOG="$ORIGIN_PATH/install.log"
mkdir $_PACKAGES_DIR
PKG="$ORIGIN_PATH/$_PACKAGES_DIR"

# Load Tar and download procedures defined as shell_utils
[ -z "$_shell_utils" ] && . ./shell_utils

# Whiptail Initialization

USE_WHIPTAIL=0
if [ "$BASH_VERSION" ] && [ "X$1" == "X" ]; then
    . ./whipdialog
    USE_WHIPTAIL=1
fi

whiptail_goodbye() {
    echo "$_POLITE_GOODBYE"    
    exit 0
}

if [ $USE_WHIPTAIL -eq 1 ]; then

    if ! WHIPTAIL \
        --title="$_WELCOME" \
        --yesno \
        "$_DIALOG_WELCOME"; then
        whiptail_goodbye
    fi
    
    _INSTALL_DIR_PROMPT=$(eval "echo \"$_INSTALL_DIR_PROMPT\"")
    if ! WHIPTAIL \
        --title="$_INSTALL_DIR_TITLE" \
        --inputbox \
        "$_INSTALL_DIR_PROMPT"; then
        whiptail_goodbye
    fi
    
    if [ "X$WHIPTAIL_RESULT" != "X" ]; then
        AT_HOME="$WHIPTAIL_RESULT"
    fi
    
    _BOOST_LIB_PROMPT=$(eval "echo \"$_BOOST_LIB_PROMPT\"")
    if ! WHIPTAIL \
        --title="$_BOOST_URL_TITLE" \
        --inputbox \
        "$_BOOST_LIB_PROMPT"; then
        whiptail_goodbye
    fi
    
    if [ "X$WHIPTAIL_RESULT" != "X" ]; then
        _BOOST_URL="$WHIPTAIL_RESULT"    
    fi
    
    _LIBTORRENT_PROMPT=$(eval "echo \"$_LIBTORRENT_PROMPT\"")
    if ! WHIPTAIL \
        --title="$_LIBTORRENT_URL_TITLE" \
        --inputbox \
        "$_LIBTORRENT_PROMPT"; then
        whiptail_goodbye
    fi
    
    if [ "X$WHIPTAIL_RESULT" != "X" ]; then
        _LIBTORRENT_URL="$WHIPTAIL_RESULT"
    fi 
      
fi

# set up log
if [ -f "$INSTALL_LOG" ]; then
    rm -f "$INSTALL_LOG"
fi
touch "$INSTALL_LOG" 2> /dev/null
if [ $? -gt 0 ]; then
    eval "echo \"$_CANNOT_WRITE_LOG\""
    INSTALL_LOG="/dev/stdout"
else
    eval "echo \"$_LOGGING_MSG\""
    echo "Detailed installation log" > "$INSTALL_LOG"
    echo "Starting at `date`" >> "$INSTALL_LOG"
fi
seelog () {
    eval "echo \"$_SEE_LOG_EXIT_MSG\""
    exit 1
}


eval "echo \"$_INSTALLING_NOW\""

# Specify Python
WITH_PYTHON=`which python${_WANT_PYTHON}`
if [ $? -gt 0 ] || [ "X$WITH_PYTHON" = "X" ]; then
    eval "echo \"$_PYTHON_NOT_FOUND\""
    exit 1
fi

# Create and check a Python virtualenv
cd "$PKG"
altdownload $_VIRTUALENV_URL
untar $_VIRTUALENV_TB
cd $_VIRTUALENV_DIR
echo $_CREATING_VIRTUALENV
"$WITH_PYTHON" virtualenv.py "$AT_HOME"  2>> "$INSTALL_LOG"

cd "$PKG"
rm -r $_VIRTUALENV_DIR
PY=$AT_HOME/bin
if [ ! -x "$PY" ]; then
    eval "echo \"$_VIRTUALENV_CREATION_FAILED\""
    exit 1
fi


# Installing Academic Torrents Bittorrent Client via pip
eval "echo \"$_PIP_INSTALL_AT\""
cd "$AT_HOME"
filename="${ORIGIN_PATH}/at_pip_files"
if [ -r "$filename" ]; then
    while read release
    do
        "${PY}/pip" install $release >> "$INSTALL_LOG" 2>&1
        
        if [ $? -gt 0 ]; then
        echo $_PIP_INSTALL_FAILED
        seelog
        exit 1
        fi
    done < "$filename"
else
    eval "echo \"$_FILE_ERROR\""
    exit 1
fi

# Install zc.buildout in the virtualenv
echo $_INSTALLING_BUILDOUT
cd "$AT_HOME"
"${PY}/pip" install zc.buildout >> "$INSTALL_LOG" 2>&1
if [ $? -gt 0 ]; then
    echo $_INSTALLING_BUILDOUT_FAILED
    seelog
    exit 1
fi

# Initialiazing buildout
echo $_INITIALIZE_BUILDOUT
"${PY}/buildout" init >> "$INSTALL_LOG" 2>&1
if [ $? -gt 0 ]; then
    echo $_INITIALIZE_BUILDOUT_FAILED
    seelog
    exit 1
fi

# Downloading bootstrap.py
eval "echo \"$_DOWNLOAD_BOOTSTRAP\""
download $_BOOTSTRAP_URL $_BOOTSTRAP_FILE

# Running bootstrap.py
echo $_RUN_BOOTSTRAP
"${PY}/python" $_BOOTSTRAP_FILE >> "$INSTALL_LOG" 2>&1
if [ $? -gt 0 ]; then
    echo $_RUN_BOOTSTRAP_FAILED
    seelog
    exit 1
fi

# Downloading new Boost
echo $_DOWNLOAD_NEW_BOOST
if [ -n "$_BOOST_URL" ]; then
    
    if [[ $_BOOST_URL =~ .+_([0-9]+)_([0-9]+)_([0-9]+)(.+) ]]; then
        _BOOST_DIR="boost_${BASH_REMATCH[1]}_${BASH_REMATCH[2]}_${BASH_REMATCH[3]}"
        _BOOST_TB="boost_${BASH_REMATCH[1]}_${BASH_REMATCH[2]}_${BASH_REMATCH[3]}${BASH_REMATCH[4]}"
        cd "$PKG"
        altdownload $_BOOST_URL
    else
        echo $_BOOST_REGEX_FAILED
        exit 1
    fi    
fi

# Function to write buildout.cfg
writeBuildout()
{
    echo "[buildout]"
    echo "parts = libffi libtorrent"
    echo
    echo "[libffi]"
    echo "recipe = zc.recipe.cmmi"
    echo "url = ${_LIBFFI_URL}"
    echo "extra_options = --prefix=${AT_HOME} --disable-static"    
    echo
    echo "[libtorrent]"
    echo "recipe = zc.recipe.cmmi"
    echo "url = ${_LIBTORRENT_URL}" 
    echo "extra_options = --enable-python-binding --disable-dependency-tracking --disable-silent-rules PYTHON=${PY}/python --prefix=${AT_HOME} --with-boost=${AT_HOME} --with-libiconv --with-boost-python=boost_python BOOST_ROOT=\"${PKG}/${_BOOST_DIR}\""
}

# Redirecting the output of writeBuildout Function to buildout.cfg
echo $_WRITING_BUILDOUT_CFG
cd "$AT_HOME"
writeBuildout > buildout.cfg

# Installing Boost
eval "echo \"$_INSTALLING_BOOST\"" 
cd "$PKG"
untar $_BOOST_TB
cd "$_BOOST_DIR"

./bootstrap.sh --with-python=${PY}/python --prefix=${AT_HOME} >> "$INSTALL_LOG" 2>&1
if [ $? -gt 0 ]; then
    echo $_BOOTSTRAP_BOOST_FAILED
    cat bootstrap.log
    seelog
    exit 1
fi

./b2 -s NO_BZIP2=1 --with-system --with-date_time --with-python --with-chrono --with-random install --prefix=${AT_HOME} >> "$INSTALL_LOG" 2>&1
if [ $? -gt 0 ]; then    
    _CONTINUE_BOOST_PROMPT=$(eval "echo \"$_CONTINUE_BOOST_PROMPT\"")
    WHIPTAIL \
    --title="$_Q_CONTINUE" \
    --yesno \
    "$_CONTINUE_BOOST_PROMPT"
    
    if [ $? -gt 0 ]; then
    seelog
    exit 1
    fi
fi

# Installing Libtorrent
eval "echo \"$_INSTALLING_LIBTORRENT\""
cd "$AT_HOME"

bin/buildout >> "$INSTALL_LOG" 2>&1

if [ $? -gt 0 ]; then
    _CONTINUE_LIBTORRENT_PROMPT=$(eval "echo \"$_CONTINUE_LIBTORRENT_PROMPT\"")
    WHIPTAIL \
    --title="$_Q_CONTINUE" \
    --yesno \
    "$_CONTINUE_LIBTORRENT_PROMPT"

    if [ $? -gt 0 ]; then
    seelog
    exit 1
    fi
fi


# Exporting LD_LIBRARY_PATH
eval "echo \"$_EXPORT_LD_LIBRARY_PATH\""
export LD_LIBRARY_PATH="${AT_HOME}"/lib
echo 'export LD_LIBRARY_PATH=$VIRTUAL_ENV/lib' >> "${AT_HOME}"/bin/activate
echo 'set -gx LD_LIBRARY_PATH $VIRTUAL_ENV/lib' >> "${AT_HOME}"/bin/activate.fish
sed -i '/deactivate ().*/a \ \ \ \ unset LD_LIBRARY_PATH' "${AT_HOME}"/bin/activate
sed -i '/function deactivate.*/a \ \ \ \ set -e LD_LIBRARY_PATH' "${AT_HOME}"/bin/activate.fish

# Exporting PKG_CONFIG_PATH
eval "echo \"$_EXPORT_PKG_CONFIG_PATH\""
export PKG_CONFIG_PATH="${AT_HOME}"/lib/pkgconfig
echo 'export PKG_CONFIG_PATH=$VIRTUAL_ENV/lib/pkgconfig' >> "${AT_HOME}"/bin/activate
echo 'set -gx PKG_CONFIG_PATH $VIRTUAL_ENV/lib/pkgconfig' >> "${AT_HOME}"/bin/activate.fish
sed -i '/deactivate ().*/a \ \ \ \ unset PKG_CONFIG_PATH' "${AT_HOME}"/bin/activate
sed -i '/function deactivate.*/a \ \ \ \ set -e PKG_CONFIG_PATH' "${AT_HOME}"/bin/activate.fish

# Testing Libtorrent
cd "$ORIGIN_PATH"
"${PY}/python" academictorrents.py
rm -r -f "$PKG"
echo $_FINISHED

# Installing Deluge Dependencies
# eval "echo \"$_INSTALL_DELUGE_DEPENDENCIES\""
# cd "$AT_HOME"
# filename="${ORIGIN_PATH}/deluge_dependencies"
# if [ -r "$filename" ]; then
#     while read dependency
#     do
#         "${PY}/pip" install $dependency >> "$INSTALL_LOG" 2>&1
#         
#         if [ $? -gt 0 ]; then
#         echo $_INSTALLING_DEPENDENCY_FAILED
#         seelog
#         exit 1
#         fi
#     done < "$filename"
# else
#     eval "echo \"$_FILE_ERROR\""
#     exit 1
# fi  

# # Function to install deluge setup.py
# installDeluge(){
#     cd "$AT_HOME/lib/python2.7"
#     mkdir dist-packages
#     cd "$PKG"
#     altdownload $_DELUGE_URL
#     untar $_DELUGE_TB
#     cd $_DELUGE_DIR
#     "${PY}/python" setup.py install "$INSTALL_LAYOUT" >> "$INSTALL_LOG" 2>&1
#     if [ $? -gt 0 ]; then
#         echo $_INSTALLING_DELUGE_FAILED
#         seelog
#         exit 1
#     fi
# }
# 
# # Function to install deluge install_data
# installDataD(){
#     "${PY}/python" setup.py install_data >> "$INSTALL_LOG" 2>&1
#     if [ $? -gt 0 ]; then
#         echo $_INSTALLING_DELUGE_FAILED
#         seelog
#         exit 1
#     fi
# }
# 
# # Installing Deluge
# echo $_INSTALLING_DELUGE
# case `uname -s` in
#     Linux) echo "You are using a Linux machine."
#            if [ -f /etc/debian_version ]; then
#                 # Also true for Ubuntu
#                 INSTALL_LAYOUT="--install-layout=deb"
#                 echo "A Debian distro type was detected."
#            elif [ -f /etc/redhat_release ]; then
#                 # Also true for variants of Fedora, CENTOS or RHEL
#                 INSTALL_LAYOUT=""
#                 echo "A Redhat distro type was detected."
#            elif [ -f /etc/SuSE-brand ] || [ -f /etc/SuSE-release ]; then
#                 INSTALL_LAYOUT=""
#                 echo "A Suse distro type was detected."
#            elif [ -f /etc/slackware-version ]; then
#                 INSTALL_LAYOUT=""
#                 echo "A Slackware distro type was detected."
#            else
#                 INSTALL_LAYOUT=""
#                 echo "Unknown distro."
#            fi
#            installDeluge
#            installDataD
#             ;;
#     Darwin) INSTALL_LAYOUT=""
#             echo "You are using a MAC"
#             installDeluge
#             ;;
#     *) INSTALL_LAYOUT=""
#             installDeluge
#             ;;
# esac
