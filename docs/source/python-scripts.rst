Running Python Scripts
======================

By using the Virtualenv
-----------------------

First, load the environment variables by activating the virtualenv::

    $ cd /path/to/virtualenv
    $ source bin/activate
    
If you the user likes to use the fish shell, then he'd rather do::

    $ cd /path/to/virtualenv
    $ . bin/activate.fish
    
This will load two environment variables:

* LD_LIBRARY_PATH
* PKG_CONFIG_PATH

These are needed by libtorrent in order to work. After that, the user
will see a modified prompt that starts with the virtualenv directory
name inside parentheses.

The user can now run a python script by either typing it in the python
shell or executing a python file while having python of the virtualenv
activated.

Python Shell
~~~~~~~~~~~~

.. code-block:: python

    (AcademicTorrentsApp) ronald@systorzo ~/A/tests> python
    Python 2.7.12 (default, Nov 19 2016, 06:48:10) 
    [GCC 5.4.0 20160609] on linux2
    Type "help", "copyright", "credits" or "license" for more information.
    
    >>> import academictorrents as at    
    >>> at.get('2269d7d1c77375aea732eea0905e370d4741575f.torrent')
    
    99.88% complete (down: 4035.0 kB/s up: 124.0 kB/s peers: 6 seeds: 6 distributed copies: 6) downloading          
     Race-Results.zip complete

.. note:: Note above the (AcademicTorrentsApp) which is part of the prompt and represents the activated
            virtualenv

Direct Python Execution
~~~~~~~~~~~~~~~~~~~~~~~

Let's say we have written a ``pythonfile.py`` as follows::

    import academictorrents as at
    at.get('2269d7d1c77375aea732eea0905e370d4741575f.torrent')
    
While having virtualenv activated, we do::

    (AcademicTorrentsApp) ronald@systorzo ~/A/tests> python pythonfile.py 
    99.79% complete (down: 3420.0 kB/s up: 102.0 kB/s peers: 5 seeds: 5 distributed copies: 5) downloading          
     Race-Results.zip complete

By using the Users Installation Method
--------------------------------------
Here we do a similar procedure for loading the environment variables.
However, the script we'll source is located in the ``HOME`` directory::

    $ source ~/.at_profile

And we can run python scripts by implementing python code in the python shell
or by writing and executing a new python file, the same way we did in the 
first method. There's no need to activate a python virtualenv here.

