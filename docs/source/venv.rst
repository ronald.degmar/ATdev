Installing on Virtualenv
========================

A --no-site-packages python2.7 virtualenv will be built which will contain the
Academic Torrents software inside.

This installation is performed by executing the install.sh script located in
the root dir of the project files. Users should run it as follows::

    $ cd /path/to/project
    
    $ ./install.sh
    
If the user is trying to install Academic Torrents as part of a more sophisticated and
bigger script which is not bash-based, he must use the following variant::

    $ ./install.sh test

i.e. add any word following the bash execution. In this case, I have used the ``test``
word because I utilized that in order to test the installer in Gitlab Continuous
Integration. This is so because the install.sh script contains a wrapper for the 
whiptail dialog which prompts and awaits for use input. However, users are not supposed
to provide input in Gitlab virtual environments. Same goes when using an automated
server provisioning tool like ansible for example; the bash script has to run
independently. When the user adds any word to the execution, the installer will run
with default values.

Advantages of using the venv bash script installer
--------------------------------------------------

Aside from all the intrinsic upsides of running Academic Torrents in a python virtualenv,
the user has the possibility to set up different custom install arguments from the default
ones already coded. Three whiptail dialogs will prompt the user for input and will perform
the custom installation according to these new arguments provided.

The customizable installation parameters are as follows:

1. Installation target directory
2. URL for Boost
3. URL for Libtorrent

Therefore, not only is it possible to change the location where the software is going to be
installed, but also to provide different versions for both Boost and Libtorrent Libraries.
It is easier then to upgrade libtorrent capabilities for example by setting up the URL of the
most recent release version.

Solving Installation Errors
---------------------------

The install.sh script logs installation output to the ``install.log`` file. This is located in
the root of the project files directory; do not look in the destination path of the installation.
Everytime, the script fails to install, the user should check the log file in order to find 
out the error(s) that caused the process to fail and fix it (them). This normally involves
installing a missing dependency package.