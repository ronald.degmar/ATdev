Installing on Users
===================

This installation method deploys the application to the ``.local`` subdirectory
inside ``HOME``. It utilizes a custom install class located in the setup.py
which means that is entirely run by python.

Two methods are available:

The first one uses pip to retrieve the available project from the python
package index. This is used so::

    $ pip install --install-option='--user' academictorrents

The second uses the setup.py directly from the project files. This is run
by doing::

    $ cd /path/to/project
    
    $ python setup.py install --user
    
Solving Installation Errors
---------------------------

Same as in the case of the venv installation method, a ``install.log`` file is
written by the installation process, so the user can check this log file in order
to identify and install any missing dependency package. Its location is the 
``.local`` subdirectory of the ``HOME`` directory. i.e. the destination path of the
users install. 
