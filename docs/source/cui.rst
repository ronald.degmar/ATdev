Console User Interface
======================

Start Academic Torrents Console User Interface by typing::

    $ at-console
    
Don't forget as always to load previously the environment variables.
Next, a multicolor screen will show up in the terminal window.
When choosing the torrent files to download in the browser window,
be aware that the program will ask the user to set up inner subfiles
priorities in case multiple files are within a single torrent file.
Only one of this type of torrent files is enough to allow the request
to set up the preference to download or not to download the inner files.

.. image:: /images/priorities.png
   :alt: npyscreen request menu
   :align: center