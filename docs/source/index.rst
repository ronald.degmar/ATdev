.. Academic Torrents documentation master file, created by
   sphinx-quickstart on Fri Apr 28 20:44:54 2017.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Academic Torrents's documentation!
=============================================

Academic Torrents is an implementation of the bittorrent protocol.
It's a server-oriented application. In other words, it makes it possible
for a user to install the application and use it without the need of a super
administrator password or root password.

This documentation is comprised of the following sections:

* :ref:`soft-arq`
* :ref:`at-install`
* :ref:`ci-dev`
* :ref:`run-app`
* :ref:`unit-test`
* :ref:`cod-std`

Information about license and sofware version can be found below as well.

* :ref:`about`

.. _soft-arq:

.. toctree::
   :maxdepth: 2
   :caption: Software Architecture
     
   platforms   
   dependencies

.. _at-install:

.. toctree::
   :maxdepth: 2
   :caption: Installation
     
   venv
   users
   
.. _ci-dev:

.. toctree::
   :maxdepth: 2
   :caption: Continuous Integration
     
   gitlab-ci

.. _run-app:

.. toctree::
   :maxdepth: 2
   :caption: Running the Application
     
   python-scripts
   r-scripts
   cui

.. _unit-test:

.. toctree::
   :maxdepth: 2
   :caption: Unit Testing
     
   test-at-unit   
   cover-run

.. _cod-std:

.. toctree::
   :maxdepth: 2
   :caption: Coding Standards
     
   pylint   
   google

.. _about:

.. toctree::
   :maxdepth: 2
   :caption: About
     
   release   
   dev-ctx  

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
