Running R Scripts
=================

It's possible to run Academic Torrent Application inside an R script.
The main requirement is to install the ``rPython`` package from R (Cran).

See the R sample script below::

    library(rPython)
    python.exec("import academictorrents as at")
    filename<-python.get("at.get('2269d7d1c77375aea732eea0905e370d4741575f.torrent')")
    print(filename)

Let's called this file ``rScript.R``. As the reader can see here, I'm loading the
rPython library in the first line. In the second line, I am executing python code
which in this case performs an import of the academictorrents api. In the third 
line, I declared a variable called filename to store the returned value of a python 
function. The python.get method makes possible to run the python at.get() function
from the academictorrents module. Finally I printed the value of the filename variable.

Let's see in this example how we run the R script by using the :doc:`users`. We do as
follows:

* Source environment variables as described in :doc:`python-scripts`
* Type R to enter into the R console interface.
* Source the ``rScript.R`` shown above::

    ronald@systorzo ~/A/tests> R
    
    R version 3.2.3 (2015-12-10) -- "Wooden Christmas-Tree"
    Copyright (C) 2015 The R Foundation for Statistical Computing
    Platform: x86_64-pc-linux-gnu (64-bit)
    
    R is free software and comes with ABSOLUTELY NO WARRANTY.
    You are welcome to redistribute it under certain conditions.
    Type 'license()' or 'licence()' for distribution details.
    
      Natural language support but running in an English locale
    
    R is a collaborative project with many contributors.
    Type 'contributors()' for more information and
    'citation()' on how to cite R or R packages in publications.
    
    Type 'demo()' for some demos, 'help()' for on-line help, or
    'help.start()' for an HTML browser interface to help.
    Type 'q()' to quit R.
    
    > source("rScript.R")
    Loading required package: RJSONIO
    99.76% complete (down: 1051.0 kB/s up: 33.0 kB/s peers: 5 seeds: 5 distributed copies: 5) downloading           
     Race-Results.zip complete
    [1] "Race-Results.zip"
    > 

.. note:: Note above there is no (AcademicTorrentsApp) prompt for an activated virtualenv which means we
          are in a :doc:`users` deployment.
