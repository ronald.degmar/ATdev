Google Standards
================

Consistent coding standards were reviewed for the Academic Torrents Project.
Code Development Style aligned to the `Google Python Style Guide`_

.. _Google Python Style Guide: https://google.github.io/styleguide/pyguide.html