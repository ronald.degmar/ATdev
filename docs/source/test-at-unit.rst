Test Classes and Test Methods
=============================

``test_at_unit.py`` file was implemented in order to run proper python unit
tests. Run all the tests in the module by executing the following::

    $ python -m unittest -v test_at_unit

TestGet Class
-------------

This class tests the get function call.

This is the preferred function to use for downloading a single torrent
without too much boilerplate code from Client Class.

Run it so::

    $ python -m unittest -v test_at_unit.TestGet

test_download_at_get
~~~~~~~~~~~~~~~~~~~~

Tries downloading a torrent from Academic Torrents
and checks if it successfully finishes downloading
until a complete message shows as final output::

    filename = at.get('2269d7d1c77375aea732eea0905e370d4741575f.torrent')
    six.print_('\n', 'Check out the downloaded file in ' + os.getcwd(), '\n')
    self.assertEqual(filename, six.text_type('Race-Results.zip'))
    self.assertTrue(os.path.isfile('Race-Results.zip'))
    
Run it so::

    $ python -m unittest -v test_at_unit.TestGet.test_download_at_get

TestClient Class
----------------

This class tests various downloading options that are only available in 
the Client Class.

Run it so::

    $ python -m unittest -v test_at_unit.TestClient

test_download_magnet
~~~~~~~~~~~~~~~~~~~~

Try downloading a magnet link from Academic Torrents and check if it 
successfully finishes downloading until a complete message shows as 
final output::

    # testing with wrong port 80000 which defaults to a valid ports range
    client = at.Client(port=80000)
    magnet_link = ('magnet:?xt=urn:btih:323a0048d87ca79b68f12a6350a57776b6a3'
                   'b7fb&tr=http%3A%2F%2Facademictorrents.com%2Fannounce.php'
                   '&tr=udp%3A%2F%2Ftracker.publicbt.com%3A80%2Fannounce&tr='
                   'udp%3A%2F%2Ftracker.openbittorrent.com%3A80%2Fannounce')
    handles = client.add_torrent([magnet_link])
    filename = client.getmodes(handles)
    # filename is not available in the case of magnet
    # link before downloading is complete
    self.assertEqual(filename[0], six.text_type('-'))
    # Download must be completed first; otherwise, getpriorities
    # will return 0
    self.assertEqual(client.getpriorities(handles[0]), 0)
    client.query_handle_sp(handles[0])
    filename = client.getmodes(handles)
    six.print_('\n', 'Check out the downloaded file in ' + os.getcwd(), '\n')
    self.assertEqual(filename[0], six.text_type('mnist.pkl.gz'))
    self.assertTrue(os.path.isfile('mnist.pkl.gz'))

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_download_magnet

test_download_http
~~~~~~~~~~~~~~~~~~

Try downloading a torrent from a URL and check if it successfully finishes
downloading until a complete message shows as final output::

    # testing with wrong port -50 which defaults to a valid ports range
    client = at.Client(port=-50)
    http_url = ('http://academictorrents.com/download/4cdade8fa3c6d451441fda'
                '7ae19bb53537b6ee21.torrent')
    handles = client.add_torrent([http_url])
    filename = client.getmodes(handles)
    # filename is not available in the case of http url before downloading
    # is complete.
    self.assertEqual(filename[0], six.text_type('-'))
    # Download must be completed first; otherwise, getpriorities will
    # return 0
    self.assertEqual(client.getpriorities(handles[0]), 0)
    client.query_handle_sp(handles[0])
    filename = client.getmodes(handles)
    six.print_('\n', 'Check out the downloaded file in ' +
               os.getcwd(), '\n')
    self.assertEqual(filename[0],
                     six.text_type('devinberg.com_471592PB.pdf'))
    self.assertTrue(os.path.isfile('devinberg.com_471592PB.pdf'))

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_download_http
    
test_set_priorities
~~~~~~~~~~~~~~~~~~~

Try downloading from a torrent file and check if the setpriorities and getpriorities()
methods set and return correct lists of priorities. Also check if the train-labels
file was correctly downloaded (1 means download and 0 means do not download)::

    client = at.Client()
    torrent_file = '42714f859770f1a9d8b27985f9f16ea17e8ba2f6.torrent'
    handles = client.add_torrent([torrent_file])
    client.setpriorities(handles[0], [0, 1, 0])
    expected_after_set = [0, 1, 0]
    self.assertListEqual(client.getpriorities(handles[0]),
                         expected_after_set)
    client.query_handle_sp(handles[0])
    six.print_('\n', 'Check out the downloaded file in ' + os.getcwd(), '\n')
    self.assertTrue(os.path.isfile(os.path.join(os.getcwd(),
                                                'files',
                                                'train-labels.tif')))
    self.assertEquals(os.path.getsize(os.path.join(os.getcwd(), 'files',
                                                   'train-labels.tif')),
                      7869573, msg='download size is incomplete')

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_set_priorities

.. note:: Note that client.setpriorities deals with all files within a torrent handler
          by providing a priority list.

test_prioritize_file
~~~~~~~~~~~~~~~~~~~~

Try downloading from a torrent file and check if the prioritize_file and getpriorities()
methods set and return correct lists of priorities. Also check if the train-labels
file was correctly downloaded (1 means download and 0 means do not download)::

    client = at.Client()
    torrent_file = '1d16994c70b7fff8bfe917f83c397b1193daee7f.torrent'
    handles = client.add_torrent([torrent_file])
    client.prioritize_file(handles[0], 0, 0)
    client.prioritize_file(handles[0], 1, 0)
    expected_after_set = [0, 0, 1]
    self.assertListEqual(client.getpriorities(handles[0]),
                         expected_after_set)
    client.query_handle_sp(handles[0])
    six.print_('\n', 'Check out the downloaded file in ' +
               os.getcwd(), '\n')
    self.assertTrue(os.path.isfile(os.path.join(os.getcwd(),
                                                'coil-20',
                                                'coil-20.ps.zip')))
    self.assertEquals(os.path.getsize(os.path.join(os.getcwd(), 'coil-20',
                                                   'coil-20.ps.zip')),
                      442932, msg='download size is incomplete')

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_prioritize_file

.. note:: Note that client.prioritize_file deals only with a file within a
          torrent handler by specifying its position index.
          
test_process_handles
~~~~~~~~~~~~~~~~~~~~

This is a npyscreen and curses libraries test, so it is only available for POSIX
systems. The goal is to check if the user can visualize actual displays for
establishing downloading priorities by using npyscreen library and for downloading
multiple files by using curses library respectively::

    client = at.Client()
    torrent_list = ['3efc53f35d49669b89039f2b4ec9de11ec1d73fd.torrent',
                    'ce990b28668abf16480b8b906640a6cd7e3b8b21.torrent']
    handles = client.add_torrent(torrent_list)
    client.run_set_priorities(handles)
    client.process_handles(handles)
    six.print_('\n', 'Check out the downloaded files in ' + os.getcwd(), '\n')

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_process_handles

.. note:: Skips unless linux

test_path_info_and_versioning
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~

This is only a look and check test. The goal here is to make sure the environment
variables for academictorrents such as LD_LIBRARY_PATH and PKG_CONFIG_PATH were
correctly set::

     at.release_info()

Run it so::

    $ python -m unittest -v test_at_unit.TestClient.test_path_info_and_versioning

TestConsole Class
-----------------

This class tests urwid menus and urwid browser window.

Run it so::

    $ python -m unittest -v test_at_unit.TestConsole
    
test_at_navigation
~~~~~~~~~~~~~~~~~~

Testing of Presentation Cover and Navigation Menu of Academic Torrents::

    console.Presentation().main()
    selected_option = console.NavigationMenu().main()
    pool_of_options = ('from .torrent', 'from URL', 'from infohash',
                       'Appearance', 'Lock Screen')
    self.assertIn(selected_option, pool_of_options,
                  msg='selected option is not in tuple')

Run it so::

    $ python -m unittest -v test_at_unit.TestConsole.test_at_navigation

.. note:: Skips unless linux

test_at_browser
~~~~~~~~~~~~~~~

Testing of Directory Browser of Academic Torrents::

    torrent_files = console.DirectoryBrowser().main()
    six.print_('These are the selected files:')
    six.print_(torrent_files)

Run it so::

    $ python -m unittest -v test_at_unit.TestConsole.test_at_browser

.. note:: Skips unless linux