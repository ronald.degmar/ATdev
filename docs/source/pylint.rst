Pylint
======

`pylint`_ is a tool for finding bugs and style problems in Python source code. 
Academic Torrents Project python files were analyzed by this tool.

To analyze a python file run::

    $ pylint filename.py

To generate a chart report run addionally::

    $ pylint filename.py --reports=y

The ``y`` means yes.

Finally, you can redirect the output of course to a .txt file and keep it for your
records. That way you can improve application code and submit proof of quality 
standards.

Like this::

    $ pylint filename.py --reports=y > ./pylint_reports/filename.txt

.. _pylint: https://pylint.readthedocs.io/en/latest/