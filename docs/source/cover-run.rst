Code Coverage
=============

`Coverage.py`_ is the python package used to perform code coverage in the
Academic Torrents Application. This package is installed by default in both
installation methods of the product.

.. _Coverage.py: https://coverage.readthedocs.io/en/coverage-4.3.4/

To use it, run it as follows::

    $ cd to path/to/tests subdirectory 
    (either in the project files or in the destination of the installation).
    
    $ coverage run -m unittest -v test_at_unit

Finally, run the following command in order to make the html pages that form
the coverage report::

    $ coverage html

A code coverage of 94% was reached by the unit tests performed. See the html 
report in the image below.

.. image:: /images/coverage.png
   :alt: Coverage Report
   :align: center
