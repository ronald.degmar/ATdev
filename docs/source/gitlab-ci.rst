Gitlab Continuous Integration
=============================

A ``.gitlab-ci.yml`` has been written and placed in the project roor folder directory.
This file will trigger a Gitlab CI Pipeline everytime a commit or push is performed to
the repository.

The script comprises 5 jobs to be performed. Four jobs consists in deploying the application
in 4 virtual machines, and the last one, called pages, will be in charge of uploading the
documentation you are reading right now to a Gitlab public folder. By watching the output of 
the gitlab embedded terminal window, the developer can check if the installation succeeded or 
failed. This is very helpful because the developer inmediately is able to see if something went
wrong with the deployment. As the reader can verify, these deployment jobs use the :doc:`venv` 
method to test in 4 linux flavors: ubuntu, debian, centos and fedora. ::

    ubuntu:16.04:
        image: ubuntu:16.04
        script:
        - apt-get update   
        - apt-get -y install wget   
        - apt-get install -y python build-essential pkg-config
        - apt-get install -y libssl-dev python-dev    
        - whereis python2.7
        - python2.7 --version
        - ./install.sh test
        artifacts:
            name: "ubuntu16"
            paths:
            - install.log
            when: always
            expire_in: 1 week
        
    debian:8:
        image: debian:8
        script:
        - apt-get update   
        - apt-get -y install wget   
        - apt-get install -y python build-essential pkg-config
        - apt-get install -y libssl-dev python-dev
        - whereis python2.7
        - python2.7 --version
        - ./install.sh test
        artifacts:
            name: "debian"
            paths:
            - install.log
            when: always
            expire_in: 1 week

    centos:7:
        image: centos:centos7
        script:
        - yum -y install wget which bzip2
        # In order to gain access to SCLs for CentOS, you need to install the CentOS Linux Software Collections release file. 
        # It is part of the CentOS Extras repository (x86_64 only) and can be installed with the following command:
        - yum -y install centos-release-scl
        - yum -y install gcc-c++ make
        #- yum -y group install 'Development Tools'
        - yum install -y python27 python-devel openssl-devel
        - which python2.7
        - whereis python2.7
        - python2.7 --version
        - ./install.sh test
        artifacts:
            name: "centos"
            paths:
            - install.log
            when: always
            expire_in: 1 week
        
    fedora:
        image: fedora:25
        script:
        - dnf -y install wget bzip2 which tar
        - dnf -y install gcc-c++ make
        # We are running a Fedora image that misses redhat-rpm-config package, so we must install it.
        - dnf -y install redhat-rpm-config
        - dnf -y install python27 python-devel openssl-devel
        - which python2.7
        - whereis python2.7
        - python2.7 --version
        - ./install.sh test
        artifacts:
            name: "fedora"
            paths:
            - install.log
            when: always
            expire_in: 1 week

    pages:
        image: ubuntu:16.04
        script:
        - apt-get update 
        - apt-get install -y python-pip python-dev    
        - pip install sphinx sphinx-autobuild sphinx_rtd_theme
        - cd docs
        - make html
        - cd ..
        - mv docs/build/html/ public/
        artifacts:
          paths:
          - public
        only:
        - master

Artifacts
---------

Logs
~~~~
After the job is finished, as a last step Gitlab uploads artifacts to the pipelines web page.
These are the ``install.log`` files we talked about in the Installation section.

Pages
~~~~~
This artifact deals with the upload of the current Academic Torrents documentation.
Sphinx is installed in an ubuntu image where html are made and moved to a public directory.

.. image:: /images/pipelines.png
   :alt: Gitlab Pipeline Display
   :align: center