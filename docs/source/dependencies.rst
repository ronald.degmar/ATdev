Software Dependencies
=====================

Dependencies built by installer
-------------------------------

Academic Torrents Application works by using the `Libtorrent`_ library.
Libtorrent has also its own dependencies: `Boost`_ and `OpenSSL`_
The versions currently used by the installer are the following:

                +------------------------+----------+
                | Dependency             | Version  |          
                +========================+==========+
                | Libtorrent             | 1.0.10   |
                +------------------------+----------+
                | Boost                  | 1.63.0   |
                +------------------------+----------+
                | OpenSSL                | 1.1.0e   |
                +------------------------+----------+

.. _Libtorrent: http://www.libtorrent.org/
.. _Boost: http://www.boost.org/
.. _OpenSSL: https://www.openssl.org/

.. note:: When installing by the :doc:`users` method, the user doesn't have to worry 
          of all these dependencies. 

.. warning:: However, when installing by the :doc:`venv` method,
             the user has to install previously OpenSSL.

Future Versions of the product will build OpenSSL when installing in virtualenv.

Platform Specific Dependencies
------------------------------

There are some packages that Academic Torrents need in order to work that cannot
be built from source or that are not recommended to build from source. Because
every linux flavor has its own name for a specific package, the user has to install
beforehand these packages by using a root password.

+---------------------+-----------------+-----------------+---------------+---------------+
| Dependency / OS     |   Ubuntu        |   Debian        |   Centos      |    Fedora     |
+=====================+=================+=================+===============+===============+
| Python              | python          | python          | python27      | python27      |
+---------------------+-----------------+-----------------+---------------+---------------+
| Python Development  | python-dev      | python-dev      | python-devel  | python-devel  |
+---------------------+-----------------+-----------------+---------------+---------------+
| OpenSSL             | libssl-dev      | libssl-dev      | openssl-devel | openssl-devel |
+---------------------+-----------------+-----------------+---------------+---------------+
| C,C++ Compilers     | build-essential | build-essential | gcc-c++, make | gcc-c++, make |
+---------------------+-----------------+-----------------+---------------+---------------+
| Package Config      | pkg-config      | pkg-config      | not required  | not required  |
+---------------------+-----------------+-----------------+---------------+---------------+

Additional Required Dependencies
--------------------------------
When installing Academic Torrents by using the :doc:`venv` method via the Bash Script, these
user has to make sure that the system has some extra software packages installed. Typically,
these packages are already installed in a system. However, when provisioning new systems from
scratch such as in the cloud, for example, this is done through pre-built images. Therefore, 
the images has to contain these packages. Otherwise, the user has to install them manually.
These are the following:

* wget
* bzip2 
* which 
* tar