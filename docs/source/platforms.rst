Supported Platforms
===================
Academic Torrents has been tested in the following Linux Operating Systems:

* Ubuntu
* Debian
* Centos
* Fedora

Moreover, it is supposed to work without any problems in any linux distro.
Although, Academic Torrents has not been tested in a MAC, the :doc:`users`
type may work as well. Microsoft Windows is not supported unfortunately.
